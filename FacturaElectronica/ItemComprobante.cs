﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacturaElectronica
{
    public class ItemComprobante
    {
        public string ItemDescripcion { get; set; }
        public decimal ItemCantidad { get; set; }
        public decimal ItemPrecioUnitario { get; set; }
    }
}
