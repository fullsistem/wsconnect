﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace FacturaElectronica
{
    public class Comprobante
    {
        public MemoryStream CrearComprobante(string nombreCopia, string tipoComprobante, string idTipoComprobanteAfip, string razonSocialEmpresa,
            string puntoVenta, string nroComprobante, DateTime fechaEmision, string domicilioComercialEmpresa, string cuitEmpresa, string ingresosBrutos,
            string condicionIvaEmpresa, DateTime fechaInicioActividades, DateTime fechaFacturadoDesde, DateTime fechaFacturadoHasta, DateTime fechaVencimentoPago,
            string tipoDocumentoCliente, string nroDocumentoCliente, string razonSocialCliente, string condicionIvaCliente, string domicilioFacturacionCliente,
            string condicionVentaCliente, List<ItemComprobante> itemList, decimal importeNeto, decimal importeExento, decimal importeImpuestos, decimal importeTotal,
            string nroCae, DateTime fechaVencimientoComprobante, string footerEmpresa, out string tituloDoc)
        {
            tituloDoc = string.Format("Comprobante_{0}_{1}_{2}", cuitEmpresa, puntoVenta, nroComprobante);

            MemoryStream stream = new MemoryStream();

            Document doc = new Document(PageSize.A4);
            PdfWriter writer = PdfWriter.GetInstance(doc, stream);
            writer.CloseStream = false;

            doc.SetMargins(50, 50, 50, 50);
            doc.Open();
            doc.AddTitle(tituloDoc);

            var barcode = CreateCodigoBarrasAfip(cuitEmpresa, idTipoComprobanteAfip, puntoVenta, nroCae, fechaVencimientoComprobante);
            var barcodeImage = Image.GetInstance(new BarcodeLib.Barcode().Encode(BarcodeLib.TYPE.Interleaved2of5, barcode, System.Drawing.Color.Black, System.Drawing.Color.White, 6000, 500), BaseColor.WHITE);

            doc.Add(CrearDatosComprobante(nombreCopia));
            doc.Add(CrearCabeceraInforme(tipoComprobante, idTipoComprobanteAfip, razonSocialEmpresa, puntoVenta, nroComprobante,
                fechaEmision, domicilioComercialEmpresa, cuitEmpresa, ingresosBrutos, condicionIvaEmpresa, fechaInicioActividades));
            doc.Add(CrearFechasInforme(fechaFacturadoDesde, fechaFacturadoHasta, fechaVencimentoPago));
            doc.Add(CrearClienteInforme(tipoDocumentoCliente, nroDocumentoCliente, razonSocialCliente, condicionIvaCliente,
                domicilioFacturacionCliente, condicionVentaCliente));
            doc.Add(CrearItemsInforme(itemList));
            doc.Add(CrearTotalesInforme(importeNeto, importeExento, importeImpuestos, importeTotal));
            doc.Add(CrearFooterInforme(nroCae, fechaVencimientoComprobante, barcode, barcodeImage, footerEmpresa));

            doc.Close();
            doc = null;

            stream.Flush();
            stream.Position = 0;

            return stream;
        }

        private PdfPTable CrearDatosComprobante(string nombreCopia)
        {
            PdfPTable tblTabla = new PdfPTable(1);
            //tblTabla.DefaultCell.Border = PdfPCell.BOTTOM_BORDER | PdfPCell.LEFT_BORDER | PdfPCell.TOP_BORDER | PdfPCell.RIGHT_BORDER;
            tblTabla.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell cell;

            Font fontEncabezado = FontFactory.GetFont("arial", 12f, Font.BOLD);

            tblTabla.SetWidths(new float[] { 500f });
            tblTabla.TotalWidth = 480f;
            tblTabla.LockedWidth = true;

            //Fila 1
            cell = new PdfPCell(new Phrase(nombreCopia.ToUpper(), fontEncabezado));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = 18f;
            tblTabla.AddCell(cell);

            //Fila de relleno
            cell = new PdfPCell(new Phrase("", fontEncabezado));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = 2f;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            return tblTabla;
        }

        private PdfPTable CrearCabeceraInforme(string tipoComprobante, string idTipoComprobanteAfip, string razonSocialEmpresa, string puntoVenta,
            string nroComprobante, DateTime fechaEmision, string domicilioComercialEmpresa, string cuitEmpresa, string ingresosBrutos,
            string condicionIvaEmpresa, DateTime fechaInicioActividades)
        {
            PdfPTable tblContenedor = new PdfPTable(1);

            tblContenedor.SetWidths(new float[] { 500f });
            tblContenedor.TotalWidth = 480f;
            tblContenedor.LockedWidth = true;

            PdfPTable tblTabla = new PdfPTable(2);
            tblTabla.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPTable tblSubtable = new PdfPTable(2);
            PdfPCell cell;

            Font fontGrande = FontFactory.GetFont("arial", 14f, Font.BOLD);
            Font fontNormal = FontFactory.GetFont("arial", 8f);
            Font fontChico = FontFactory.GetFont("arial", 6f);

            float fixedSmallHeight = 10f, fixedNormalHeight = 12f, fixedBigHeight = 18f;

            tblTabla.SetWidths(new float[] { 250f, 250f });
            tblTabla.TotalWidth = 480f;
            tblTabla.LockedWidth = true;

            PdfPTable tblComprobante = new PdfPTable(1);
            tblComprobante.SetWidths(new float[] { 500f });
            tblComprobante.TotalWidth = 480f;
            tblComprobante.LockedWidth = true;

            //Fila 1
            cell = new PdfPCell(new Phrase(tipoComprobante.ToUpper(), fontGrande));
            cell.FixedHeight = fixedBigHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblComprobante.AddCell(cell);

            //Fila 2
            cell = new PdfPCell(new Phrase("COD. " + idTipoComprobanteAfip.ToUpper(), fontChico));
            cell.FixedHeight = fixedSmallHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblComprobante.AddCell(cell);

            //Fila contenedora de la tabla de datos
            cell = new PdfPCell(tblComprobante);
            cell.Colspan = 2;
            tblTabla.AddCell(cell);

            //Colspan del anterior

            //Fila 3
            cell = new PdfPCell(new Phrase("Razón Social: " + razonSocialEmpresa.ToUpper(), fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Punto Venta: " + puntoVenta.PadLeft(4, '0') + " Nro. Comprobante: " + nroComprobante.PadLeft(8, '0'), fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            //Fila 4
            //Rowspan de la fila anterior

            cell = new PdfPCell(new Phrase("Fecha Emisión: " + fechaEmision.ToString("dd/MM/yyyy"), fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            //Fila 5
            cell = new PdfPCell(new Phrase("Domicilio Comercial: " + domicilioComercialEmpresa, fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("CUIT: " + cuitEmpresa, fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            //Fila 6
            //Rowspan de la fila anterior

            cell = new PdfPCell(new Phrase("Ingresos Brutos: " + ingresosBrutos, fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            //Fila 7
            cell = new PdfPCell(new Phrase("Condición frente al IVA: " + condicionIvaEmpresa, fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Inicio Actividades: " + fechaInicioActividades.ToString("dd/MM/yyyy"), fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            //Fila de relleno
            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = 2f;
            cell.Border = 0;
            cell.Colspan = 2;
            tblTabla.AddCell(cell);

            //Fila contenedora de la tabla de datos
            cell = new PdfPCell(tblTabla);
            tblContenedor.AddCell(cell);

            //Fila de relleno
            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = 2f;
            cell.Border = 0;
            tblContenedor.AddCell(cell);

            return tblContenedor;
        }

        private PdfPTable CrearFechasInforme(DateTime fechaFacturadoDesde, DateTime fechaFacturadoHasta, DateTime fechaVencimentoPago)
        {
            PdfPTable tblTabla = new PdfPTable(1);
            tblTabla.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell cell;

            //Font fontEncabezado = FontFactory.GetFont("arial", 10f, Font.BOLD);
            Font fontEncabezado = FontFactory.GetFont("arial", 10f);

            tblTabla.SetWidths(new float[] { 500f });
            tblTabla.TotalWidth = 480f;
            tblTabla.LockedWidth = true;

            //Fila 1
            string texto = "";
            texto += "Período Facturado Desde: " + fechaFacturadoDesde.ToString("dd/MM/yyyy");
            texto += "   Hasta: " + fechaFacturadoHasta.ToString("dd/MM/yyyy");
            texto += "   Fecha Vto. para el pago: " + fechaVencimentoPago.ToString("dd/MM/yyyy");

            cell = new PdfPCell(new Phrase(texto, fontEncabezado));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = 16f;
            tblTabla.AddCell(cell);

            //Fila de relleno
            cell = new PdfPCell(new Phrase("", fontEncabezado));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = 2f;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            return tblTabla;
        }

        private PdfPTable CrearClienteInforme(string tipoDocumentoCliente, string nroDocumentoCliente, string razonSocialCliente, string condicionIvaCliente,
            string domicilioFacturacionCliente, string condicionVentaCliente)
        {
            PdfPTable tblContenedor = new PdfPTable(1);

            tblContenedor.SetWidths(new float[] { 500f });
            tblContenedor.TotalWidth = 480f;
            tblContenedor.LockedWidth = true;

            PdfPTable tblTabla = new PdfPTable(2);
            tblTabla.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell cell;

            Font fontNormal = FontFactory.GetFont("arial", 8f);

            float fixedNormalHeight = 12f;

            tblTabla.SetWidths(new float[] { 250f, 250f });
            tblTabla.TotalWidth = 480f;
            tblTabla.LockedWidth = true;

            //Fila 1
            cell = new PdfPCell(new Phrase(tipoDocumentoCliente + ": " + nroDocumentoCliente, fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Apellido y Nombre / Razón Social: " + razonSocialCliente, fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            //Fila 2
            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            //Rowspan de la fila anterior

            //Fila 3
            cell = new PdfPCell(new Phrase("Condición frente al IVA: " + condicionIvaCliente, fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Domicilio: " + domicilioFacturacionCliente, fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            //Fila 4
            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            //Rowspan de la fila anterior

            //Fila 5
            cell = new PdfPCell(new Phrase("Condición de venta: " + condicionVentaCliente, fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Border = 0;
            tblTabla.AddCell(cell);

            //Fila contenedora de la tabla de datos
            cell = new PdfPCell(tblTabla);
            tblContenedor.AddCell(cell);

            //Fila de relleno
            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = 2f;
            cell.Border = 0;
            tblContenedor.AddCell(cell);

            return tblContenedor;
        }

        private PdfPTable CrearItemsInforme(List<ItemComprobante> itemList)
        {
            PdfPTable tblContenedor = new PdfPTable(1);

            tblContenedor.SetWidths(new float[] { 500f });
            tblContenedor.TotalWidth = 480f;
            tblContenedor.LockedWidth = true;

            PdfPTable tblTabla = new PdfPTable(4);
            tblTabla.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPTable tblSubtable = new PdfPTable(2);
            PdfPCell cell;

            decimal montoFila = 0;
            Font fontNormal = FontFactory.GetFont("arial", 8f);
            float fixedNormalHeight = 12f, fixedLastHeight = 380f;
            var cellHeaderColor = new iTextSharp.text.BaseColor(181, 182, 183);

            tblTabla.SetWidths(new float[] { 380f, 40f, 40f, 40f });
            tblTabla.TotalWidth = 480f;
            tblTabla.LockedWidth = true;

            //Fila 1
            cell = new PdfPCell(new Phrase("Producto / Servicio", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = cellHeaderColor;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Cantidad", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = cellHeaderColor;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Precio Unitario", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = cellHeaderColor;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Subtotal", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.BackgroundColor = cellHeaderColor;
            tblTabla.AddCell(cell);

            //Fila 2
            if (itemList != null)
            {
                foreach (var item in itemList)
                {
                    cell = new PdfPCell(new Phrase(item.ItemDescripcion, fontNormal));
                    cell.FixedHeight = fixedNormalHeight;
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.VerticalAlignment = Element.ALIGN_CENTER;
                    tblTabla.AddCell(cell);

                    cell = new PdfPCell(new Phrase(item.ItemCantidad.ToString(), fontNormal));
                    cell.FixedHeight = fixedNormalHeight;
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_CENTER;
                    cell.VerticalAlignment = Element.ALIGN_CENTER;
                    tblTabla.AddCell(cell);

                    cell = new PdfPCell(new Phrase(item.ItemPrecioUnitario.ToString(), fontNormal));
                    cell.FixedHeight = fixedNormalHeight;
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    cell.VerticalAlignment = Element.ALIGN_CENTER;
                    tblTabla.AddCell(cell);

                    montoFila = item.ItemCantidad * item.ItemPrecioUnitario;
                    cell = new PdfPCell(new Phrase(montoFila.ToString(), fontNormal));
                    cell.FixedHeight = fixedNormalHeight;
                    cell.Border = 0;
                    cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    cell.VerticalAlignment = Element.ALIGN_CENTER;
                    tblTabla.AddCell(cell);
                }
            }

            //Fila 3
            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.FixedHeight = fixedLastHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_LEFT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.FixedHeight = fixedLastHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.FixedHeight = fixedLastHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.FixedHeight = fixedLastHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            //Fila contenedora de la tabla de datos
            cell = new PdfPCell(tblTabla);
            tblContenedor.AddCell(cell);

            //Fila de relleno
            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = 2f;
            cell.Border = 0;
            tblContenedor.AddCell(cell);

            return tblContenedor;
        }

        private PdfPTable CrearTotalesInforme(decimal importeNeto, decimal importeExento, decimal importeImpuestos, decimal importeTotal)
        {
            PdfPTable tblContenedor = new PdfPTable(1);

            tblContenedor.SetWidths(new float[] { 500f });
            tblContenedor.TotalWidth = 480f;
            tblContenedor.LockedWidth = true;

            PdfPTable tblTabla = new PdfPTable(3);
            tblTabla.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPTable tblSubtable = new PdfPTable(2);
            PdfPCell cell;

            Font fontNormal = FontFactory.GetFont("arial", 8f);
            float fixedNormalHeight = 16f;

            tblTabla.SetWidths(new float[] { 350f, 100f, 50f });
            tblTabla.TotalWidth = 480f;
            tblTabla.LockedWidth = true;

            //Fila 1
            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Subtotal: $", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase((importeNeto + importeExento).ToString(), fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            //Fila 2
            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Importe Otros Tributos: $", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase(importeImpuestos.ToString(), fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            //Fila 3
            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Importe Total: $", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase(importeTotal.ToString(), fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            //Fila contenedora de la tabla de datos
            cell = new PdfPCell(tblTabla);
            tblContenedor.AddCell(cell);

            //Fila de relleno
            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = 2f;
            cell.Border = 0;
            tblContenedor.AddCell(cell);

            return tblContenedor;
        }

        private PdfPTable CrearFooterInforme(string nroCae, DateTime fechaVencimientoComprobante, string barcode, Image barcodeImage, string footerEmpresa = "")
        {
            PdfPTable tblContenedor = new PdfPTable(1);

            tblContenedor.SetWidths(new float[] { 500f });
            tblContenedor.TotalWidth = 480f;
            tblContenedor.LockedWidth = true;

            PdfPTable tblTabla = new PdfPTable(2);
            tblTabla.HorizontalAlignment = Element.ALIGN_CENTER;
            PdfPCell cell;

            Font fontNormal = FontFactory.GetFont("arial", 8f);
            Font fontChica = FontFactory.GetFont("arial", 6f);

            float fixedNormalHeight = 14f, fixedBarcodeHeight = 22f, fixedFooterHeight = 40f;

            tblTabla.SetWidths(new float[] { 350f, 150f });
            tblTabla.TotalWidth = 480f;
            tblTabla.LockedWidth = true;

            //Fila 1
            cell = new PdfPCell(new Phrase("Comprobante Autorizado", fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Nro. CAE: " + nroCae, fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            //Fila 2
            cell = new PdfPCell(barcodeImage, true);
            cell.FixedHeight = fixedBarcodeHeight;
            cell.Border = 0;
            cell.Rowspan = 2;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            cell = new PdfPCell(new Phrase("Fecha de Vto. de CAE: " + fechaVencimientoComprobante.ToString("dd/MM/yyyy"), fontNormal));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.Rowspan = 3;
            cell.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            //Fila 3
            //Rowspan del anterior

            //Rowspan del anterior

            //Fila 4
            cell = new PdfPCell(new Phrase(barcode, fontChica));
            cell.FixedHeight = fixedNormalHeight;
            cell.Border = 0;
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            tblTabla.AddCell(cell);

            //Rowspan del anterior

            //Fila contenedora de la tabla de datos
            cell = new PdfPCell(tblTabla);
            tblContenedor.AddCell(cell);

            //Fila de relleno
            cell = new PdfPCell(new Phrase("", fontNormal));
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.FixedHeight = 2f;
            cell.Border = 0;
            cell.Colspan = 2;
            tblContenedor.AddCell(cell);

            if (!string.IsNullOrWhiteSpace(footerEmpresa))
            {
                //Fila 2
                cell = new PdfPCell(new Phrase(footerEmpresa, fontNormal));
                cell.FixedHeight = fixedFooterHeight;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                cell.VerticalAlignment = Element.ALIGN_CENTER;
                tblContenedor.AddCell(cell);
            }

            return tblContenedor;
        }

        private string CreateCodigoBarrasAfip(string cuitEmpresa, string idTipoComprobanteAfip, string puntoVenta, string nroCae, DateTime fechaVencimientoComprobante)
        {
            string barcode = cuitEmpresa +
                             idTipoComprobanteAfip.PadLeft(2, '0') +
                             puntoVenta.PadLeft(4, '0') +
                             nroCae +
                             fechaVencimientoComprobante.ToString("yyyyMMdd");

            return barcode + GetDigitoVerificador(barcode);
        }

        private int GetDigitoVerificador(string valor)
        {
            if (!string.IsNullOrWhiteSpace(valor))
            {
                int totalPares = 0, totalImpares = 0;
                //Se considera para efectuar el cálculo el siguiente ejemplo:
                //01234567890

                //Etapa 1: Comenzar desde la izquierda, sumar todos los caracteres ubicados en las posiciones impares. 
                //0 + 2 + 4 + 6 + 8 + 0 = 20
                //Etapa 3: Comenzar desde la izquierda, sumar todos los caracteres que están ubicados en las posiciones pares. 
                //1 + 3 + 5 + 7 + 9 = 25

                for (var i = 0; i < valor.Length; i++)
                {
                    if (i % 2 == 0)
                        totalImpares += (int)Char.GetNumericValue(valor[i]);
                    else
                        totalPares += (int)Char.GetNumericValue(valor[i]);
                }

                //Etapa 2: Multiplicar la suma obtenida en la etapa 1 por el número 3.
                //20 x 3 = 60
                //Etapa 4: Sumar los resultados obtenidos en las etapas 2 y 3.
                //60 + 25 = 85
                var parcial = (totalImpares * 3) + totalPares;

                //Etapa 5: Buscar el menor número que sumado al resultado obtenido en la etapa 4 dé un número múltiplo de 10.Este será el valor del dígito verificador del módulo 10.
                //85 + 5 = 90

                for (var i = 0; i < 10; i++)
                {
                    if ((parcial + i) % 10 == 0)
                        return i;
                }
            }

            return 0;
        }
    }
}
