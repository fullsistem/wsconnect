﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacturaElectronica
{
	public class Cliente
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		public void SolicitarCAE(long cuit, string sign, string token, int comprobante, int comprobanteNumero, int puntoDeVenta, int concepto, int documentoTipo, long documentoNumero,
			DateTime fecha, DateTime servicioDesde, DateTime servicioHasta, DateTime vencimientoPago, decimal importe, decimal importeNeto, decimal importeExento, decimal iva21,
			decimal iva10, decimal pv1, decimal pp1, string pd1, decimal pv2, decimal pp2, string pd2, out string cae, out string vencimiento, out string observacion)
		{
			var svc = new Service.ServiceSoapClient();
			var auth = new Service.FEAuthRequest();
			auth.Cuit = cuit;
			auth.Sign = sign;
			auth.Token = token;

			var req = new Service.FECAERequest();
			req.FeCabReq = new Service.FECAECabRequest();
			req.FeCabReq.CantReg = 1;
			req.FeCabReq.CbteTipo = comprobante;
			req.FeCabReq.PtoVta = puntoDeVenta;
			req.FeDetReq = new Service.FECAEDetRequest[1];
			req.FeDetReq[0] = new Service.FECAEDetRequest();
			req.FeDetReq[0].Concepto = concepto;
			req.FeDetReq[0].DocTipo = documentoTipo;
			req.FeDetReq[0].DocNro = documentoNumero;
			req.FeDetReq[0].CbteDesde = comprobanteNumero;
			req.FeDetReq[0].CbteHasta = comprobanteNumero;
			req.FeDetReq[0].CbteFch = fecha.ToString("yyyyMMdd");
			req.FeDetReq[0].ImpTotal = Math.Round(Convert.ToDouble(importe),2);
			req.FeDetReq[0].ImpTotConc = 0;
			req.FeDetReq[0].ImpNeto = Math.Round(Convert.ToDouble(importeNeto),2);
			//iva10 = importeNeto;
			req.FeDetReq[0].ImpOpEx = Math.Round(Convert.ToDouble(importeExento),2);
			req.FeDetReq[0].ImpIVA = Math.Round(Convert.ToDouble(iva21) * .21 + Convert.ToDouble(iva10) * 0.105,2);
			req.FeDetReq[0].ImpTrib = Convert.ToDouble(Math.Round(pv1,2)+Math.Round(pv2,2));
			if (concepto != 1)
			{
				req.FeDetReq[0].FchServDesde = servicioDesde.ToString("yyyyMMdd");
				req.FeDetReq[0].FchServHasta = servicioHasta.ToString("yyyyMMdd");
				req.FeDetReq[0].FchVtoPago = vencimientoPago.ToString("yyyyMMdd");
			}
			req.FeDetReq[0].MonId = "PES";
			req.FeDetReq[0].MonCotiz = 1;

			int ivas = iva21 > 0 ? 1 : 0 + iva10 > 0 ? 1 : 0;
			if (ivas > 0)
			{
				req.FeDetReq[0].Iva = new Service.AlicIva[ivas];
				if (iva21 > 0)
				{
					req.FeDetReq[0].Iva[0] = new Service.AlicIva();
					req.FeDetReq[0].Iva[0].Id = 5;
					req.FeDetReq[0].Iva[0].BaseImp = (double)iva21;
					req.FeDetReq[0].Iva[0].Importe = Math.Round( (double)iva21 * 0.21,2);
				}
				if (iva10 > 0)
				{
					req.FeDetReq[0].Iva[ivas - 1] = new Service.AlicIva();
					req.FeDetReq[0].Iva[ivas - 1].Id = 4;
					req.FeDetReq[0].Iva[ivas - 1].BaseImp = (double)iva10;
					req.FeDetReq[0].Iva[ivas - 1].Importe = Math.Round( (double)iva10 * 0.105,2);
				}
			}
			int trib = (pv1 > 0 ? 1 : 0) + (pv2 > 0 ? 1 : 0);
			if (trib > 0)
			{
				req.FeDetReq[0].Tributos = new Service.Tributo[trib];
				if (pv1 > 0)
				{
					req.FeDetReq[0].Tributos[0] = new Service.Tributo();
					req.FeDetReq[0].Tributos[0].Id = 7;
					req.FeDetReq[0].Tributos[0].Alic = (double)pp1;
					req.FeDetReq[0].Tributos[0].BaseImp = (double)(importeNeto + importeExento);
					req.FeDetReq[0].Tributos[0].Importe = (double)pv1;
				}
				if (pv2 > 0)
				{
					req.FeDetReq[0].Tributos[trib - 1] = new Service.Tributo();
					req.FeDetReq[0].Tributos[trib - 1].Id = 7;
					req.FeDetReq[0].Tributos[trib - 1].Alic = (double)pp2;
					req.FeDetReq[0].Tributos[trib - 1].BaseImp = (double)(importeNeto + importeExento);
					req.FeDetReq[0].Tributos[trib - 1].Importe = (double)pv2;
				}
			}


			string notas="";
			var resp1 = svc.FECAESolicitar(auth, req);

			if (resp1 != null && resp1.FeDetResp != null && resp1.FeDetResp.Count() > 0 && resp1.FeDetResp[0].Observaciones != null)
			{
				notas = string.Join("*", resp1.FeDetResp[0].Observaciones.Select(i => string.Format("{0}-{1}", i.Code, i.Msg)));
			}

			if (resp1.Errors == null || resp1.Errors.Count() == 0)
			{
				cae = resp1.FeDetResp.First().CAE;
				vencimiento = resp1.FeDetResp.First().CAEFchVto;
				observacion = "";
			}
			else
			{
				cae = "0";
				vencimiento = new DateTime().ToShortDateString();
				observacion = string.Join("*", resp1.Errors.Select(i => string.Format("{0}-{1}", i.Code, i.Msg)));
			}
			if (!string.IsNullOrEmpty(notas))
			{
				if (!string.IsNullOrEmpty(observacion))
					observacion += "*";
				observacion += notas;
			}
		}

		public void test(long cuit, string sign, string token)
		{
			var svc = new Service.ServiceSoapClient();
			var auth = new Service.FEAuthRequest();
			auth.Cuit = cuit;
			auth.Sign = sign;
			auth.Token = token;
			var numero = svc.FEParamGetTiposTributos(auth);
			var comp = svc.FECompConsultar(auth, new Service.FECompConsultaReq { CbteNro = 33349, CbteTipo = 6, PtoVta = 2 });
			foreach (var item in numero.ResultGet)
			{
				logger.Debug(string.Format("{0};{1}", item.Id, item.Desc));
			}

		}

		public void ConsultarEstado(long cuit, int punto, int tipo, string sign, string token, out int numero)
		{
			var svc = new Service.ServiceSoapClient();
			var auth = new Service.FEAuthRequest();
			auth.Cuit = cuit;
			auth.Sign = sign;
			auth.Token = token;
			var result = svc.FECompUltimoAutorizado(auth, punto, tipo);
			numero = result.CbteNro;

		}

		public void ConsultarCae(long cuit, int punto, int tipo, int numero, string sign, string token, out string cae, out string vto)
		{
			var svc = new Service.ServiceSoapClient();
			var auth = new Service.FEAuthRequest();
			auth.Cuit = cuit;
			auth.Sign = sign;
			auth.Token = token;
			var query =new Service.FECompConsultaReq();
			query.CbteNro = numero;
			query.CbteTipo= tipo;
			query.PtoVta = punto;
			var result = svc.FECompConsultar(auth,query);
			if (result.ResultGet != null)
			{
				cae = result.ResultGet.CodAutorizacion;
				vto = result.ResultGet.FchVto;
			}
			else
			{
				cae = "";
				vto = "";
			}

		}

        public void ValidarComprobante(long cuit, string sign, string token, string modo, long cuitEmisor,string tipoDocumentoReceptor, string documentoReceptor, int punto, int tipo, int numero, DateTime fecha, double importe, string cae, out string resultado)
        {
            var svc = new ConstatacionService.ServiceSoapClient();
            var auth = new ConstatacionService.CmpAuthRequest();
            auth.Cuit = cuit;
            auth.Sign = sign;
            auth.Token = token;
            var query = new ConstatacionService.CmpDatos();
            query.CbteModo = modo;
            query.CuitEmisor = cuitEmisor;
            query.PtoVta = punto;
            query.CbteTipo = tipo;
            query.CbteNro = numero;
            query.DocTipoReceptor = tipoDocumentoReceptor;
            query.DocNroReceptor = documentoReceptor;
            query.CbteFch = fecha.ToString("yyyyMMdd");
            query.ImpTotal = importe;
            query.CodAutorizacion = cae;

            resultado = "";
            var result = svc.ComprobanteConstatar(auth, query);
            if (result != null)
            {
                resultado = result.Resultado;
            }
        }

		public void SolicitarCAEA(long cuit, int periodo, short orden, string sign, string token, out string caea, out string fechaVigDesde, out string fechaVigHasta, out string fechaTopeInf, out Dictionary<int, string> errores)
		{
			caea = "";
			fechaVigDesde = "";
			fechaVigHasta = "";
			fechaTopeInf = "";
			errores = null;

			var svc = new Service.ServiceSoapClient();
			var auth = new Service.FEAuthRequest();
			auth.Cuit = cuit;
			auth.Sign = sign;
			auth.Token = token;

			var result = svc.FECAEASolicitar(auth, periodo, orden);

			if (result.Errors != null && result.Errors.Any())
				errores = result.Errors.GroupBy(g => g.Code).ToDictionary(x => x.Key, y => string.Join("*", y.Select(z => z.Msg)));

			if (result.ResultGet != null)
			{
				caea = result.ResultGet.CAEA;
				fechaVigDesde = result.ResultGet.FchVigDesde;
				fechaVigHasta = result.ResultGet.FchVigHasta;
				fechaTopeInf = result.ResultGet.FchTopeInf;
			}
		}

		public void ConsultarCaea(long cuit, int periodo, short orden, string sign, string token, out string caea, out string fechaVigDesde, out string fechaVigHasta, out string fechaTopeInf, out Dictionary<int, string> errores)
		{
			caea = "";
			fechaVigDesde = "";
			fechaVigHasta = "";
			fechaTopeInf = "";
			errores = null;

			var svc = new Service.ServiceSoapClient();
			var auth = new Service.FEAuthRequest();
			auth.Cuit = cuit;
			auth.Sign = sign;
			auth.Token = token;

			var result = svc.FECAEAConsultar(auth, periodo, orden);

			if (result.Errors != null && result.Errors.Any())
				errores = result.Errors.GroupBy(g => g.Code).ToDictionary(x => x.Key, y => string.Join("*", y.Select(z => z.Msg)));

			if (result.ResultGet != null)
			{
				caea = result.ResultGet.CAEA;
				fechaVigDesde = result.ResultGet.FchVigDesde;
				fechaVigHasta = result.ResultGet.FchVigHasta;
				fechaTopeInf = result.ResultGet.FchTopeInf;
			}
		}

		public void InformarCAEA(string sign, string token, long cuit, int puntoVenta, int tipoComprobante,

			out string resultado, out string reproceso, out string fechaProceso, out Dictionary<int, string> errores, out Dictionary<int, string> resultadoComprobantes)
		{
			resultado = "";
			reproceso = "";
			fechaProceso = "";
			errores = null;
			resultadoComprobantes = null;

			var svc = new Service.ServiceSoapClient();

			var auth = new Service.FEAuthRequest();
			auth.Cuit = cuit;
			auth.Sign = sign;
			auth.Token = token;

			var query = new Service.FECAEARequest();
			query.FeDetReq = new Service.FECAEADetRequest[1];
			
			query.FeCabReq = new Service.FECAEACabRequest()
			{
				CantReg = query.FeDetReq.Length,
				CbteTipo = tipoComprobante,
				PtoVta = puntoVenta
			};

			var result = svc.FECAEARegInformativo(auth, query);

			if (result.Errors != null && result.Errors.Any())
				errores = result.Errors.GroupBy(g => g.Code).ToDictionary(x => x.Key, y => string.Join("*", y.Select(z => z.Msg)));

			if (result.FeCabResp != null)
			{
				resultado = result.FeCabResp.Resultado;
				reproceso = result.FeCabResp.Reproceso;
				fechaProceso = result.FeCabResp.FchProceso;
			}

			if (result.FeDetResp != null)
			{
				resultadoComprobantes = new Dictionary<int, string>();
				//result.FeDetResp.FirstOrDefault().
			}
		}
	}
}
