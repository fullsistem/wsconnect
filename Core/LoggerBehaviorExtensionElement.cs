﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
	public class LoggerBehaviorExtensionElement : BehaviorExtensionElement
	{
		public override Type BehaviorType
		{
			get { return typeof(LoggerEndpointBehavior); }
		}

		protected override object CreateBehavior()
		{
			return new LoggerEndpointBehavior();
		}  
	}
}
