﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
	public class MessageInspector : IClientMessageInspector
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();


		public void AfterReceiveReply(ref Message reply, object correlationState)
		{
			var buffer = reply.CreateBufferedCopy(Int32.MaxValue);
			reply = buffer.CreateMessage();
			StringBuilder sb = new StringBuilder();
			using (System.Xml.XmlWriter xw = System.Xml.XmlWriter.Create(sb))
			{
				reply.WriteMessage(xw);
				xw.Close();
			}
			logger.Debug(string.Format("Received:\n{0}", sb.ToString()));
		}
		public object BeforeSendRequest(ref Message request, IClientChannel channel)
		{
			var buffer = request.CreateBufferedCopy(Int32.MaxValue);
			request = buffer.CreateMessage();
			//StringBuilder sb = new StringBuilder();
			//using (System.Xml.XmlWriter xw = System.Xml.XmlWriter.Create(sb))
			//{
			//	//request.WriteMessage(xw);
			//	xw.Close();
			//}
			//logger.Debug(string.Format("Sending:\n{0}", sb.ToString()));

			return null;
		}
	}
}
