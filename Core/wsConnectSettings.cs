﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Core
{
	public class wsConnectSettings : ConfigurationSection
	{
		private static wsConnectSettings settings= ConfigurationManager.GetSection("wsConnectSettings") as wsConnectSettings;

		public static wsConnectSettings Settings
		{
			get
			{
				return settings;
			}
		}

		[ConfigurationProperty("Cuit", IsRequired = true)]
		public Int64 Cuit
		{
			get { return (Int64)this["Cuit"]; }
			set { this["Cuit"] = value; }
		}

		[ConfigurationProperty("TipoIntegracion", DefaultValue = 1, IsRequired = true)]
		[IntegerValidator(MinValue = 1, MaxValue = 2)]
		public int TipoIntegracion
		{
			get { return (int)this["TipoIntegracion"]; }
			set { this["TipoIntegracion"] = value; }
		}


		[ConfigurationProperty("Certificado", IsRequired = true)]
		public string Certificado
		{
			get { return (string)this["Certificado"]; }
			set { this["Certificado"] = value; }
		}

		[ConfigurationProperty("Carpeta", IsRequired = true)]
		public string Carpeta
		{
			get { return (string)this["Carpeta"]; }
			set { this["Carpeta"] = value; }
		}

		[ConfigurationProperty("Conexion", IsRequired = true)]
		public string Conexion
		{
			get { return (string)this["Conexion"]; }
			set { this["Conexion"] = value; }
		}

		[ConfigurationProperty("Consulta", IsRequired = true)]
		public string Consulta
		{
			get { return (string)this["Consulta"]; }
			set { this["Consulta"] = value; }
		}

		[ConfigurationProperty("Actualizacion", IsRequired = true)]
		public string Actualizacion
		{
			get { return (string)this["Actualizacion"]; }
			set { this["Actualizacion"] = value; }
		}

		[ConfigurationProperty("Licencia", IsRequired = true)]
		public string Licencia
		{
			get { return (string)this["Licencia"]; }
			set { this["Licencia"] = value; }
		}
	}
}
