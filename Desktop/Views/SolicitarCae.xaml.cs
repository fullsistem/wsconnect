﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Desktop.Views
{
	/// <summary>
	/// Interaction logic for SolicitarCae.xaml
	/// </summary>
	public partial class SolicitarCae : UserControl
	{
        private readonly BackgroundWorker worker = new BackgroundWorker();
        private Timer ticker;

        public SolicitarCae()
		{
			InitializeComponent();
            ticker = new Timer(tick, null, 0, 1000);
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
        }

        private void tick(object state)
        {
            this.Dispatcher.Invoke(() =>
            {
                Numero.Text = Service.Proxy.Numero.ToString();
            });
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
			Service.Proxy.SolicitarCAE();
            
        }

        private void worker_RunWorkerCompleted(object sender,RunWorkerCompletedEventArgs e)
        {
            Consultar.IsEnabled = true;
            Consultar.Content = "Consultar";
            MessageBox.Show("Proceso finalizado?","Informacion",MessageBoxButton.OK,MessageBoxImage.Information);
        }
        private async void Button_Click(object sender, RoutedEventArgs e)
		{
			var cae = "";
			var vto = "";
			Consultar.IsEnabled = false;
			Consultar.Content = "Consultando...";
            worker.RunWorkerAsync();

            //Service.Proxy.ConsultarCae(0, model.Punto, model.Tipo, model.Numero, out cae, out vto);
        }

	}
}
