﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Desktop.Views
{
	/// <summary>
	/// Interaction logic for ConsultaCae.xaml
	/// </summary>
	public partial class ConsultaCae : UserControl
	{
		public ConsultarCaeModel model { get; set; }
		public ConsultaCae()
		{
			InitializeComponent();
			model = new ConsultarCaeModel();
			DataContext = model;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			var cae = "";
			var vto = "";
			Service.Proxy.ConsultarCae(0, model.Punto, model.Tipo, model.Numero, out cae, out vto);
			model.Cae = cae;
			try
			{
				model.Vencimiento = DateTime.ParseExact(vto, "yyyyMMdd", null);
			}
			catch (Exception)
			{
			}
		}
	}
}
