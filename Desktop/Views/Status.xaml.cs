﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Desktop.Views
{
	/// <summary>
	/// Interaction logic for Status.xaml
	/// </summary>
	public partial class Status : UserControl
	{
		public StatusModel model { get; set; }
		public Status()
		{
			InitializeComponent();
			model = new StatusModel();
			DataContext = model;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			int numero;
			Service.Proxy.ConsultarEstado(0, model.Punto, model.Tipo, out numero);
			model.Numero = numero;
			Service.Proxy.test();

		}
	}
}
