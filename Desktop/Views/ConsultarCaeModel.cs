﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Desktop.Views
{
	public class ConsultarCaeModel : INotifyPropertyChanged
	{
		private int punto { get; set; }
		private int tipo { get; set; }
		private int numero { get; set; }
		private DateTime vencimiento { get; set; }
		private string cae { get; set; }
		private HashSet<KeyValuePair<int, string>> comprobanteTipoList;

		public HashSet<KeyValuePair<int, string>> ComprobanteTipoList
		{
			get { return comprobanteTipoList; }
			set { comprobanteTipoList = value; }
		}
		
		public event PropertyChangedEventHandler PropertyChanged;
		private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public int Punto
		{
			get { return punto;}
			set { punto = value;
			NotifyPropertyChanged();
			}
		}
		public int Tipo
		{
			get { return tipo; }
			set
			{
				tipo = value;
				NotifyPropertyChanged();
			}
		}
		public int Numero
		{
			get { return numero; }
			set
			{
				numero= value;
				NotifyPropertyChanged();
			}
		}
		public string Cae
		{
			get { return cae; }
			set
			{
				cae = value;
				NotifyPropertyChanged();
			}
		}
		public DateTime Vencimiento
		{
			get { return vencimiento; }
			set
			{
				vencimiento = value;
				NotifyPropertyChanged();
			}
		}

		public ConsultarCaeModel()
		{
			ComprobanteTipoList = new HashSet<KeyValuePair<int, string>>();
			ComprobanteTipoList.Add(new KeyValuePair<int, string>(1, "Factura A"));
			ComprobanteTipoList.Add(new KeyValuePair<int, string>(6, "Factura B"));
		}

	}
}
