﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login
{
	public class Cliente
	{
		public string Token { get; set; }
		public string Sign { get; set; }
		public DateTime Expiration { get; set; }
		public string ObtenerTicket(string servicio, string certificado)
		{
			var ticket = new LoginTicket();
			var respuesta = ticket.ObtenerLoginTicketResponse(servicio, "", certificado, false);
			Token = ticket.Token;
			Sign = ticket.Sign;
			Expiration = ticket.ExpirationTime;
			return respuesta;
		}
	}
}
