﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public class Ticket
	{
		public Servicio.Valores Servicio { get; set; }
		public string Sign { get; set; }
		public string Token { get; set; }
		public DateTime Expiration { get; set; }
	}
}
