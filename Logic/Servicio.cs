﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
	public class Servicio
	{
		public static Dictionary<Valores, Servicio> Datos { get; set; }
		public int Id { get; set; }
		public string Codigo { get; set; }
		public string Nombre { get; set; }
		public string Descripcion { get; set; }
		public enum Valores
		{
			WSFE = 1,
            WSCDC = 2
		}
		public static Dictionary<Valores, Servicio> Lista()
		{
			if (Datos == null)
			{
				Datos = new Dictionary<Valores, Servicio>();
				Datos.Add(Valores.WSFE, new Servicio() { Id = (int)Valores.WSFE, Codigo = "wsfe", Nombre = "Factura Electronica v1" });
                Datos.Add(Valores.WSCDC, new Servicio() { Id = (int)Valores.WSCDC, Codigo = "wscdc", Nombre = "Constatación de Comprobantes" });
			}
			return Datos;

		}
	}
}
