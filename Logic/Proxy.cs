﻿using Core;
using Integracion;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Logic
{
    public class Proxy
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();
		public string Certificado { get; set; }
		public Dictionary<Servicio.Valores, Ticket> Tickets { get; set; }
		private Timer ticker;
		private wsConnectSettings Settings = wsConnectSettings.Settings;
		private FactoryDAC Integracion { get; set; }
        public int Numero { get; set; }

        public Proxy()
		{
			string saltValue = "s@1tValue";
			string hashAlgorithm = "SHA1";
			int passwordIterations = 2;
			string initVector = "@1B2c3D4e5F6g7H8";
			int keySize = 256;

			Tickets = new Dictionary<Servicio.Valores, Ticket>();
			Certificado = Settings.Certificado;

			Integracion = new FactoryDAC((FactoryDAC.Type)Settings.TipoIntegracion);
			try
			{
				//var plainText = RijndaelSimple.Decrypt
				//		(
				//			Settings.Licencia,
				//			Settings.Cuit.ToString(),
				//			saltValue,
				//			hashAlgorithm,
				//			passwordIterations,
				//			initVector,
				//			keySize
				//		);

				//if (plainText != "LICENCIA")
				//	throw new LicenseException(this.GetType(), this, "Licencia invalida");
			}
			catch (Exception)
			{
				throw new LicenseException(this.GetType(),this, "Licencia invalida")  ;
			}
		}

        public Ticket Login(Servicio.Valores servicio)
		{
			if (File.Exists("Tickets.txt"))
			{
				var lines = File.ReadAllLines("tickets.txt");
				foreach (var item in lines)
				{
					var valores = item.Split('|');
					var valor = new DateTime(1900, 1, 1);
					DateTime.TryParse(valores[1], out valor);
					if (valor > DateTime.Now)
					{
						Tickets.Add(servicio, new Ticket() { Servicio = servicio, Sign = valores[2], Token = valores[3], Expiration = valor });
						return Tickets[servicio];
					}
				}
			}
			var login = new Login.Cliente();
			var ticket = login.ObtenerTicket(Servicio.Lista()[servicio].Codigo, Certificado);
			Tickets.Add(servicio, new Ticket() { Servicio = servicio, Sign = login.Sign, Token = login.Token, Expiration = login.Expiration });
			logger.Debug(string.Format("{0};{1};{2};{3};", servicio, login.Sign, login.Token, login.Expiration));
			File.AppendAllLines("tickets.txt", new string[] { string.Format("{0}|{1}|{2}|{3}", servicio, login.Expiration, login.Sign, login.Token) });
			
			return Tickets[servicio];
		}

        public void SolicitarCAE()
		{
			Ticket ticket = null;
			if(Tickets.ContainsKey(Servicio.Valores.WSFE))
			{
				if (Tickets[Servicio.Valores.WSFE].Expiration > DateTime.Now)
					ticket = Tickets[Servicio.Valores.WSFE];
				else
					Tickets.Remove(Servicio.Valores.WSFE);
			}
			if (ticket == null)
				ticket = Login(Servicio.Valores.WSFE);
			var fe = new FacturaElectronica.Cliente();
			//fe.test(Settings.Cuit, ticket.Sign, ticket.Token);
			string cae="i";
			string vencimiento;
			DateTime vencimientoValor;
			string error;
			int numero;
			Integracion.Worker.GetAll().ForEach(i =>
			{
				try
				{
					logger.Error(string.Format("{0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} | {10} | {11} | {12} | {13} | {14} | {15} | {16} | {17} | {18} | {19} | {20}", i.Tipo, i.Numero, i.PuntoDeVenta, i.Concepto, i.DocumentoTipo, i.DocumentoNumero, i.Fecha, i.ServicioDesde, i.ServicioHasta, i.VencimientoPago, i.Importe, i.ImporteNeto, i.ImporteExento, i.Iva21, i.Iva105, i.Percepcion1V, i.Percepcion1P, i.Percepcion1D, i.Percepcion2V, i.Percepcion2P, i.Percepcion2D));
					cae = "";
					vencimiento = "";
					error = "";
					ConsultarEstado(0, i.PuntoDeVenta, i.Tipo, out numero);
					i.Numero = numero + 1;
                    Numero = i.Numero;
					fe.SolicitarCAE(Settings.Cuit, ticket.Sign, ticket.Token, i.Tipo, i.Numero, i.PuntoDeVenta, i.Concepto, i.DocumentoTipo, i.DocumentoNumero, i.Fecha, i.ServicioDesde, i.ServicioHasta, i.VencimientoPago, i.Importe, i.ImporteNeto, i.ImporteExento, i.Iva21, i.Iva105, i.Percepcion1V, i.Percepcion1P, i.Percepcion1D, i.Percepcion2V, i.Percepcion2P, i.Percepcion2D, out cae, out vencimiento, out error);
					i.Cae = cae;
					try
					{
						i.Vencimiento = DateTime.ParseExact(vencimiento, "yyyyMMdd", null);
					}
					catch (Exception)
					{
						i.Vencimiento = new DateTime(1900, 1, 1);
						Console.WriteLine("no se puede convertir la fecha");
					}
					i.Observacion = error;
					Console.WriteLine(error);
					logger.Debug(i.Data());
					Integracion.Worker.Update(i);

				}
				catch (Exception ex)
				{
					logger.Error(ex);
				}
			});
		}

        public void ConsultarCae(long cuit, int punto, int tipo, int numero, out string cae, out string vto)
		{
			Ticket ticket = null;
			if (Tickets.ContainsKey(Servicio.Valores.WSFE))
			{
				if (Tickets[Servicio.Valores.WSFE].Expiration > DateTime.Now)
					ticket = Tickets[Servicio.Valores.WSFE];
				else
					Tickets.Remove(Servicio.Valores.WSFE);
			}
			if (ticket == null)
				ticket = Login(Servicio.Valores.WSFE);

			var fe = new FacturaElectronica.Cliente();
			fe.ConsultarCae(Settings.Cuit, punto, tipo, numero, ticket.Sign, ticket.Token, out cae, out vto);
		}
		
		public void ConsultarEstado(long cuit, int punto, int tipo, out int numero)
		{
			Ticket ticket = null;
			if (Tickets.ContainsKey(Servicio.Valores.WSFE))
			{
				if (Tickets[Servicio.Valores.WSFE].Expiration > DateTime.Now)
					ticket = Tickets[Servicio.Valores.WSFE];
				else
					Tickets.Remove(Servicio.Valores.WSFE);
			}
			if (ticket == null)
				ticket = Login(Servicio.Valores.WSFE);

			var fe = new FacturaElectronica.Cliente();
			fe.ConsultarEstado(Settings.Cuit, punto, tipo, ticket.Sign, ticket.Token, out numero);
		}

        public void ValidarComprobante(long cuit, string sign, string token, string modo, long cuitEmisor, string tipoDocumentoReceptor, string documentoReceptor, int punto, int tipo, int numero, DateTime fecha, double importe, string cae, out string resultado)
        {
            if (string.IsNullOrEmpty( token))
            {
                Ticket ticket = null;
                if (Tickets.ContainsKey(Servicio.Valores.WSCDC))
                {
                    if (Tickets[Servicio.Valores.WSCDC].Expiration > DateTime.Now)
                        ticket = Tickets[Servicio.Valores.WSCDC];
                    else
                        Tickets.Remove(Servicio.Valores.WSCDC);
                }
                if (ticket == null)
                    ticket = Login(Servicio.Valores.WSCDC);
                cuit = Settings.Cuit;
                sign = ticket.Sign;
                token = ticket.Token;
            }
            var fe = new FacturaElectronica.Cliente();
            fe.ValidarComprobante(cuit, sign, token, modo, cuitEmisor,tipoDocumentoReceptor,documentoReceptor, punto, tipo, numero, fecha, importe, cae, out resultado);
        }

        public void test()
		{
			Ticket ticket = null;
			if (Tickets.ContainsKey(Servicio.Valores.WSFE))
			{
				if (Tickets[Servicio.Valores.WSFE].Expiration > DateTime.Now)
					ticket = Tickets[Servicio.Valores.WSFE];
				else
					Tickets.Remove(Servicio.Valores.WSFE);
			}
			if (ticket == null)
				ticket = Login(Servicio.Valores.WSFE);

			var fe = new FacturaElectronica.Cliente();
			fe.test(Settings.Cuit, ticket.Sign, ticket.Token);
		}

    }
}
