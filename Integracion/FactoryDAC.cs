﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integracion
{
	public class FactoryDAC
	{
		public enum Type
		{
			Database = 1,
			File = 2
		}
		public GenericDAC Worker { get; set; }
		public FactoryDAC(Type value)
		{
			switch (value)
			{
				case Type.Database:
					Worker = new DatabaseDAC();
					break;
				case Type.File:
					Worker = new FileDAC();
					break;
				default:
					break;
			}
		}
	}
}
