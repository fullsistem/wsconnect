﻿using Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integracion
{
	public class FileDAC : GenericDAC
	{
		private string carpeta;
		public FileDAC()
		{
			carpeta = wsConnectSettings.Settings.Carpeta;
		}
		public override List<DTO.Comprobante> GetAll()
		{
			var result = new List<DTO.Comprobante>();
			Directory.EnumerateFiles(carpeta).ToList().ForEach(i => {
				var values = File.ReadAllText(i).Split(';');
				result.Add(new DTO.Comprobante()
				{
					Tipo = Converter.ComprobanteTipo(values[0]),
					PuntoDeVenta = int.Parse(values[1]),
					Concepto = Converter.ComprobanteTipo(values[2]),
					DocumentoTipo = Converter.DocumentoTipo(values[3]),
					DocumentoNumero = long.Parse(values[4]),
					Fecha = DateTime.Parse(values[5]),
					Importe = Decimal.Parse(values[6]),
					Iva21 = Decimal.Parse(values[7]),
					Iva105 = Decimal.Parse(values[8]),
				});
			});
			return result;
		}

		public override bool Update(DTO.Comprobante item)
		{
			throw new NotImplementedException();
		}
	}
}
