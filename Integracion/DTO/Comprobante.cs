﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integracion.DTO
{
	public class Comprobante
	{
		public int Tipo { get; set; }
		public int PuntoDeVenta { get; set; }
		public int Numero { get; set; }
		public int Concepto { get; set; }
		public int DocumentoTipo { get; set; }
		public long DocumentoNumero { get; set; }
		public DateTime Fecha { get; set; }
		public Decimal Importe { get; set; }
		public Decimal ImporteNeto { get; set; }
		public Decimal ImporteExento { get; set; }
		public Decimal Iva21 { get; set; }
		public Decimal Iva105 { get; set; }
		public DateTime ServicioDesde { get; set; }
		public DateTime ServicioHasta { get; set; }
		public DateTime VencimientoPago { get; set; }

		public Decimal Percepcion1V { get; set; }
		public string Percepcion1D { get; set; }
		public Decimal Percepcion1P { get; set; }
		public Decimal Percepcion2V { get; set; }
		public string Percepcion2D { get; set; }
		public Decimal Percepcion2P { get; set; }



		public string Cae { get; set; }
		public DateTime Vencimiento { get; set; }
		public string Referencia { get; set; }
		public string Observacion { get; set; }
		public string VencimientoAnsi
		{
			get
			{
				return Vencimiento.ToString("yyyyMMdd");
			}
		}
		public string Data()
		{
			return string.Format("{0};{1};{2};{3};{4};{5};{6};", Referencia, Tipo, PuntoDeVenta, Numero, Cae, Vencimiento, Observacion);
		}

	}
}
