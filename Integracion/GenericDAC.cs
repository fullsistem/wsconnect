﻿using Core;
using Integracion.DTO;
using Integracion.ValueConverter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integracion
{
	public abstract class GenericDAC
	{
		protected BaseConverter Converter;
		protected wsConnectSettings Settings = wsConnectSettings.Settings;
		
		public GenericDAC()
		{
			Converter = new BaseConverter();
		}
		public abstract List<Comprobante> GetAll();
		public abstract bool Update(Comprobante item);
	}
}
