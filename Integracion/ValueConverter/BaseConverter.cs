﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integracion.ValueConverter
{
	public class BaseConverter
	{
		public int ComprobanteTipo(object value)
		{
			//return 6;
			return (int)value;
		}
		public int DocumentoTipo(object value)
		{
			return (int)value;
		}
		public string Moneda(object value)
		{
			return value.ToString();
		}
		public int Concepto(object value)
		{
			return (int)value;
		}
	}
}
