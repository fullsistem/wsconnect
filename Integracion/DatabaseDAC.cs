﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Helpers;
namespace Integracion
{
	public  class DatabaseDAC:GenericDAC
	{
		public override List<DTO.Comprobante> GetAll()
		{
			var result = new List<DTO.Comprobante>();

			using (SqlConnection con = new SqlConnection(Settings.Conexion))
			{
				con.Open();
				using (SqlCommand command = new SqlCommand(Settings.Consulta, con))
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						var item = new DTO.Comprobante();
						item.Tipo = Converter.ComprobanteTipo(reader.GetInt32(0));
							item.PuntoDeVenta = reader.GetInt32(1);
							item.Numero = reader.GetInt32(2);
							item.Concepto = Converter.Concepto(reader.GetInt32(3));
							item.DocumentoTipo = Converter.DocumentoTipo(reader.GetInt32(4));
							item.DocumentoNumero = long.Parse( reader.GetString(5));
							//item.Fecha =DateTime.ParseExact(reader.GetString(6), "d/M/yyyy",null);
							item.Fecha =reader.GetDateTime(6);
							item.ServicioDesde = reader.GetDateTime(7);
							item.ServicioHasta = reader.GetDateTime(8);
							//item.VencimientoPago = DateTime.ParseExact(reader.GetString(9), "dd/M/yyyy", null);
							item.VencimientoPago = reader.GetDateTime(9);
							item.Importe = Math.Round( reader.GetDecimal(10),2);
							item.ImporteNeto = Math.Round( reader[13] as decimal? ??0,2);
							item.ImporteExento = Math.Round(reader[14] as decimal? ?? 0, 2);
							item.Iva21 = Math.Round(reader[11] as decimal? ?? 0, 2);
							item.Iva105 = Math.Round(reader[12] as decimal? ?? 0, 2);
							item.Percepcion1V = Math.Round(reader[15] as decimal? ?? 0, 2);
							item.Percepcion1D = reader[16] as string;
							item.Percepcion2V = Math.Round(reader[17] as decimal? ?? 0, 2);
							item.Percepcion2D = reader[18] as string;
							item.Referencia = reader[19].ToString();
							item.Percepcion1P = Math.Round(reader[20] as decimal? ?? 0, 2);
							item.Percepcion2P = Math.Round(reader[21] as decimal? ?? 0, 2);
							result.Add(item);
					}
				}
			}
			return result;
		}

		public override bool Update(DTO.Comprobante item)
		{
			using (SqlConnection con = new SqlConnection(Settings.Conexion))
			{
				con.Open();
				using (SqlCommand command = new SqlCommand(Settings.Actualizacion.FormatWith(item), con))
				{
					command.ExecuteNonQuery();
				}
			}
			return true;
		}
	}
}
